import datetime
import unittest
import warnings
from collections import Counter

import pandas as pd
import pandas.api.types as ptypes

from Common.DatabaseProviders.mongo_provider import MongoProvider
from Scripts.criteria_calculator import (
    load_data,
    calculate_tf_idf,
    allocate_to_clusters_silhouette,
    calculate_current_mass,
    calculate_future_mass,
    calculate_success
)
from utils import MONGO_CONNECTION_STRING


class CriteriaCalculatorTests(unittest.TestCase):
    def test_load_data(self):
        database_provider = MongoProvider(MONGO_CONNECTION_STRING)
        result_df = load_data(database_provider)
        self.assertIsInstance(result_df, pd.DataFrame)
        self.assertFalse(result_df.empty)
        self.assertIn('publication', result_df.columns)
        self.assertIn('number_of_citations', result_df.columns)
        self.assertIn('organisation_name', result_df.columns)
        self.assertTrue(ptypes.is_datetime64_dtype(result_df['publication']))
        self.assertTrue(ptypes.is_string_dtype(result_df['organisation_name']))

        for column in result_df[result_df.columns.difference(['publication', 'organisation_name'])].columns:
            self.assertTrue(ptypes.is_float_dtype(result_df[column]))

    def test_calculate_tf_idf_correct(self):
        input_list = [
            [1, 1, 0, 1, 1, 1, 1, 1, 0],
            [1, 0, 1, 1, 1, 1, 1, 0, 1]
        ]

        expected_list = [
            [0, 0.043, 0, 0, 0, 0, 0, 0.043, 0],
            [0, 0, 0.043, 0, 0, 0, 0, 0, 0.043]
        ]

        calculate_tf_idf(input_list)

        self.assertEqual(input_list, expected_list)

    def test_allocate_to_clusters_silhouette_correct(self):
        input_list = [[0, 0, 0.043, 0, 0, 0, 0, 0, 0.043]] * 7
        input_list.extend([
            [0, 0, 0, 0, 0.043, 0, 0.043, 0, 0],
            [0.043, 0, 0, 0.043, 0, 0, 0, 0, 0]
        ])

        expected_labels_count = [1, 7]

        warnings.simplefilter('ignore')
        result_labels = allocate_to_clusters_silhouette(input_list)
        labels_counter = Counter(result_labels)

        for value in labels_counter.values():
            self.assertIn(value, expected_labels_count)

    def test_allocate_to_clusters_silhouette_not_enough_data(self):
        input_list = [[0, 0, 0.043, 0, 0, 0, 0, 0, 0.043]]
        self.assertRaises(ValueError, allocate_to_clusters_silhouette, input_list)

    def test_calculate_current_mass(self):
        input_list = [1, 1, 0, 2, 2, 1, 1, 1, 2, 1]
        expected_current_mass = [10, 10, 2, 5, 5, 10, 10, 10, 5, 10]
        result_current_mass = calculate_current_mass(input_list)

        self.assertEqual(expected_current_mass, result_current_mass)

        for elem in result_current_mass:
            self.assertIsInstance(elem, int)
            self.assertTrue(0 <= elem <= 10)

    def test_calculate_future_mass(self):
        input_data = [0, 0, 1]
        input_dates = [datetime.datetime(2020, 1, 6), datetime.datetime(2020, 10, 10), datetime.datetime(2021, 2, 20)]

        result_future_mass = calculate_future_mass(input_data, input_dates)

        self.assertEqual(len(input_data), len(result_future_mass))

        for elem in result_future_mass:
            self.assertIsInstance(elem, int)
            self.assertTrue(0 <= elem <= 10)

    def test_calculate_success(self):
        input_list = [0, 2, 5, 6, 7, 10, -1, -5]
        expected_success = [False, False, False, True, True, True, False, False]

        result_success = calculate_success(input_list)

        self.assertEqual(expected_success, result_success)

    def test_calculate_success_incorrect_data_type(self):
        input_list = ['0', '2', '5', '6', '7', '10', '-1', '-5']
        self.assertRaises(TypeError, calculate_success, input_list)


def main():
    unittest.main()


if __name__ == '__main__':
    main()
