import json
import math
from collections import Counter
from collections import defaultdict
from datetime import datetime
from typing import List, Union

import numpy as np
import pandas as pd
from dateutil.relativedelta import relativedelta
from matplotlib import pyplot as plt
from sklearn.cluster import KMeans
from sklearn.metrics import mean_absolute_percentage_error
from sklearn.metrics import silhouette_score
from statsmodels.tsa.arima.model import ARIMA

from Common.DatabaseProviders.clickhouse_provider import ClickhouseProvider
from Common.DatabaseProviders.mongo_provider import MongoProvider
from Common.database import Database
from Common.database_provider import DatabaseProvider
from utils import MONGO_CONNECTION_STRING, CLICKHOUSE_CONNECTION_STRING


def main():
    database_selection = int(
        input('Choose the DB to work with:\n'
              '\t1. ClickHouse;\n'
              '\t2. MongoDB.\n'
              '?: '))

    if database_selection == 1:
        database_provider = ClickhouseProvider(CLICKHOUSE_CONNECTION_STRING)
    elif database_selection == 2:
        database_provider = MongoProvider(MONGO_CONNECTION_STRING)
    else:
        print('Invalid menu item entered.')
        exit(1)

    df = load_data(database_provider)
    print(df)

    publications = df.pop('publication')
    citations = df.pop('number_of_citations')
    organisation_names = df.pop('organisation_name')

    calculate_tf_idf(df.values)
    clusters = allocate_to_clusters_silhouette(df.values, plot=True)

    current_mass = calculate_current_mass(clusters, plot=True)
    future_mass = calculate_future_mass(clusters, publications.values, plot=True)
    success = calculate_success(citations.values.tolist(), plot=True)

    print(clusters)
    print(current_mass)
    print(future_mass)
    print(success)

    row_index = -1

    while row_index == -1:
        patent_id = input('Enter the patent id to see calculated criteria: ')

        try:
            row_index = df.index.get_loc(patent_id)
        except KeyError:
            print('There is no patent with such id, please, try again.\n')

    patent_info = {
        'patent_id': patent_id,
        'organisation_name': organisation_names[row_index],
        'criteria': [
            {
                'name': 'mass in the current period',
                'result': current_mass[row_index]
            },
            {
                'name': 'mass in the future',
                'result': future_mass[row_index]
            },
            {
                'name': 'success',
                'result': success[row_index]
            }
        ]
    }

    print(json.dumps(patent_info, indent=4, ensure_ascii=False))


def load_data(database_provider: DatabaseProvider) -> pd.DataFrame:
    """
    Загружает термы всех патентов и устанавливает количество их употребления в каждом патенте.
    :param database_provider: Провайдер базы данных.
    :return: Таблица, содержащая количество употреблений термов в каждом патенте,
    дату публикации и количество цитирований.
    """

    database = Database(database_provider)
    return database.get_table_for_analysis(5000)


def calculate_tf_idf(data: List[List[int]]):
    """
    Приводит каждый элемент списка к TF-IDF.
    :param data: Двумерный список целых чисел.
    """

    count = np.count_nonzero(data, axis=0)
    document_terms_count = np.sum(data, axis=1)

    data_len = len(data)

    for y in range(data_len):
        for x in range(len(data[y])):
            if document_terms_count[y] != 0:
                data[y][x] = round(data[y][x] / document_terms_count[y] * math.log10(data_len / count[x]), 3)


def allocate_to_clusters_silhouette(data: List[List[float]], plot: bool = False) -> List[int]:
    """
    Кластеризует векторы при помощи метода силуэта.
    :param data: Двумерный список вещественных чисел.
    :param plot: Построить ли график анализа силуэта от выбранного кластера.
    :return: Список меток, привязанных к каждому вектору.
    """

    clusters_count = {'min': 2, 'max': 10}
    silhouette_avg = []

    for num_clusters in range(clusters_count['min'], clusters_count['max']):
        now = datetime.now().strftime('%H:%M:%S')
        print(f'[{now}] - num_clusters: {num_clusters}')
        kmeans = KMeans(n_clusters=num_clusters, n_init=10).fit(data)
        silhouette_avg.append(silhouette_score(data, kmeans.labels_))

    # TODO: Проверить производительность сохранения labels в массив при итерациях цикла,
    # чтобы не делать лишний раз кластеризацию снова
    optimal_k = silhouette_avg.index(max(silhouette_avg)) + clusters_count['min']
    print(f'Clusters count: {optimal_k}')

    kmeans = KMeans(n_clusters=optimal_k, n_init=10).fit(data)

    if plot:
        plt.plot(range(clusters_count['min'], clusters_count['max']), silhouette_avg, 'bx-')
        plt.xlabel('Values of K')
        plt.ylabel('Silhouette score')
        plt.title('Silhouette analysis For Optimal k')
        plt.show()

        plot_distribution(
            data=kmeans.labels_,
            title='Number of patents in clusters',
            xlabel='Clusters',
            ylabel='Count'
        )

    return kmeans.labels_


def calculate_current_mass(data: List, plot: bool = False) -> List[float]:
    """
    Рассчитывает массовость на основе употребления элементов.
    :param data: Список значений.
    :param plot: Построить ли график распределения текущей массовости патентов.
    :return: Список, который содержит массовость употребления элементов, округленных к десятибалльной шкале.
    """

    cluster_distribution = Counter(data)
    max_cluster_patents_count = max(cluster_distribution.values())
    current_mass = [round(cluster_distribution[item] / max_cluster_patents_count * 10) for item in data]

    if plot:
        plot_distribution(
            data=current_mass,
            title='Current mass distribution',
            xlabel='Current mass',
            ylabel='Count'
        )

    return current_mass


def calculate_future_mass(data: List[int], dates: List[datetime], quarters_to_predict: int = 4, plot: bool = False) \
        -> List[float]:
    """
    Рассчитывает массовость на будущий период на основе употребления элементов за прошлые периоды.
    :param data: Список целочисленных значений.
    :param dates: Список дат, в которые были употреблены элементы.
    :param quarters_to_predict: Количество кварталов, на которое необходимо составить прогноз.
    :param plot: Построить ли график предсказания значимости.
    :return: Список, который содержит массовость употребления элементов на будущий период,
    округленных к десятибалльной шкале.
    """

    if len(data) != len(dates):
        raise AttributeError('Data and dates list sizes are different')

    MONTHS_IN_QUARTER = 3

    now = datetime.now()
    future_mass = {}
    grouped_by_data = defaultdict(list)

    for k, v in zip(data, pd.to_datetime(dates).to_period('Q')):
        grouped_by_data[k].append(v)

    for k, v in grouped_by_data.items():
        full_dates = pd.date_range(
            start=min(v).to_timestamp('Q'),
            end=now + relativedelta(months=quarters_to_predict * MONTHS_IN_QUARTER),
            freq='Q').to_period('Q').tolist()

        periods_with_count = {date: 0 for date in full_dates}

        for item in v:
            periods_with_count[item] += 1

        training_values = list(periods_with_count.values())

        model = ARIMA(training_values, order=(1, 1, 1)).fit()
        future_mass[k] = round(model.forecasts[0][-1], 6)

        mape = mean_absolute_percentage_error([value + 1 for value in training_values], [value + 1 for value in model.forecasts[0]])
        print(f'MAPE for {k} cluster: {mape}')

        if plot:
            quarters_df = pd.DataFrame(
                periods_with_count.values(),
                index=periods_with_count.keys(),
                columns=['count'])

            model = ARIMA(quarters_df['count'], order=(1, 1, 1)).fit()

            from statsmodels.graphics.tsaplots import plot_predict
            fig, ax = plt.subplots()
            ax = quarters_df.plot(ax=ax)
            plot_predict(model, quarters_df.index[0], '2024Q1', ax=ax)
            plt.xlabel('Quarters')
            plt.ylabel('Future mass')
            plt.title('Future mass forecast')
            plt.show()

    max_future_mass = max(future_mass.values())
    return list(map(lambda x: round(future_mass[x] / max_future_mass * 10), data))


def calculate_success(data: List[int], plot: bool = False) -> List[bool]:
    """
    Рассчитывает успешность патента в информационном поле.
    :param data: Количество цитирований патента.
    :param plot: Построить ли график распределения патентов на успешные и неуспешные.
    :return: Список, разбитый на два класса цитируемости: высокая и низкая. True, если высокая, иначе - False.
    """

    success = [item > 5 for item in data]

    if plot:
        plot_distribution(
            data=success,
            title='Number of successful patents',
            xlabel='Success',
            ylabel='Count'
        )

    return success


def plot_distribution(data: List[Union[int, bool]], title: str = '', xlabel: str = '', ylabel: str = '') -> None:
    counter = Counter(data)
    counter = sorted(counter.items())
    values = [elem[0] for elem in counter]
    count = [elem[1] for elem in counter]

    f, ax = plt.subplots()

    plt.bar(values, count)
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    ax.set_xticks(range(values[0], values[-1] + 1))

    plt.show()


if __name__ == '__main__':
    main()
