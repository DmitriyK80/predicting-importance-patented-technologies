import pandas as pd

from Common.database_provider import DatabaseProvider
from Models.patent_model import PatentModel


class Database:
    def __init__(self, database_provider: DatabaseProvider) -> None:
        self.__database_provider = database_provider

    @property
    def database_provider(self) -> DatabaseProvider:
        return self.__database_provider

    @database_provider.setter
    def database_provider(self, database_provider: DatabaseProvider) -> None:
        self.__database_provider = database_provider

    def ensure_db_and_table_created(self) -> None:
        """
        Удостоверяет, что БД и таблица созданы.
        """
        self.__database_provider.ensure_db_and_table_created()

    def check_if_patent_exists(self, patent_id: str) -> bool:
        """
        Проверяет, содержится ли патент в БД.
        :param patent_id: Уникальный идентификатор патента.
        :return: True, если содержится патент в БД, иначе - False.
        """
        return self.__database_provider.check_if_patent_exists(patent_id)

    def insert_patent(self, patent: PatentModel):
        """
        Добавляет патент в БД.
        :param patent: Патент.
        """
        self.__database_provider.insert_patent(patent)

    def get_table_for_analysis(self, limit: int = 500) -> pd.DataFrame:
        """
        Загружает термы всех патентов и устанавливает количество их употребления в каждом патенте.
        :param limit: Количество патентов в сформированном патентном массиве.
        :return: Таблица, содержащая количество употреблений термов в каждом патенте,
        дату публикации и количество цитирований.
        """
        return self.__database_provider.get_table_for_analysis(limit)
