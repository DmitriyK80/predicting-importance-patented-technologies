from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By

from Common.patent_web_driver import PatentWebDriver


class GooglePatentWebDriver(PatentWebDriver):
    def __init__(self, path_to_driver: str = 'chromedriver.exe') -> None:
        super().__init__(path_to_driver)

    def get_patent_html(self, patent_link: str) -> str:
        self._driver.get(patent_link)

        # Expand the list of concepts
        try:
            self._driver.find_element(By.LINK_TEXT, 'Show all concepts from the description section').click()
        except NoSuchElementException:
            pass

        return self._driver.page_source
