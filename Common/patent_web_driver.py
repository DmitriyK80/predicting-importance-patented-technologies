from abc import ABC, abstractmethod

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.chrome.service import Service


class PatentWebDriver(ABC):
    def __init__(self, path_to_driver: str = 'chromedriver.exe') -> None:
        """
        Создать вебдрайвер для парсинга html-страниц.
        :param path_to_driver: Путь к файлу вебдрайвера.
        :return: Сконфигурированный вебдрайвер.
        """
        options = Options()
        options.headless = True
        self._driver = webdriver.Chrome(service=Service(path_to_driver), options=options)

    @abstractmethod
    def get_patent_html(self, patent_link: str) -> str:
        """
        Получить html-представление патента.
        :param patent_link: Ссылка на патент.
        :return: Строковая html-страница патента.
        """
        pass

    def quit(self):
        self._driver.quit()
