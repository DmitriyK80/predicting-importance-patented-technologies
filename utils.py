import os

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
CHROMEDRIVER_PATH = f'{ROOT_DIR}/Utils/chromedriver.exe'

CLICKHOUSE_CONNECTION_STRING = 'localhost'
MONGO_CONNECTION_STRING = 'mongodb://localhost:27017'
